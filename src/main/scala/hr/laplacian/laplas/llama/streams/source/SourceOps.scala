package hr.laplacian.laplas.llama.streams.source

import akka.NotUsed
import akka.stream.scaladsl.Source

trait SourceOps
{
  implicit class ConcatLazyOps[E, M](val src: Source[E, M]) {
    def concatLazy[M1](src2: => Source[E, M1]): Source[E, NotUsed] =
      Source(List(() => src, () => src2)).flatMapConcat(_())
  }
}

object SourceOps extends SourceOps
