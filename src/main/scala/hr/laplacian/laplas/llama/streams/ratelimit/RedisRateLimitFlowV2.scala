package hr.laplacian.laplas.llama.streams.ratelimit

import akka.NotUsed
import akka.stream.scaladsl.Flow
import hr.laplacian.laplas.redis.RedisService

import scala.concurrent.duration._

object RedisRateLimitFlowV2 {

  def apply[T](key: String, limit: Long, interval: FiniteDuration, tickRate: FiniteDuration = 1.second)(
      implicit redisService: RedisService): Flow[T, T, NotUsed] =
    Flow
      .fromGraph(RedisParallelismLimitInFlow[T](key, limit, interval, tickRate)(redisService))
      .map(_.elem)
}
