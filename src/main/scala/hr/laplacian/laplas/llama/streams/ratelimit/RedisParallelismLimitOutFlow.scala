package hr.laplacian.laplas.llama.streams.ratelimit

import akka.NotUsed
import akka.stream.scaladsl.Flow
import hr.laplacian.laplas.redis.RedisService

object RedisParallelismLimitOutFlow {

  def apply[T](implicit redisService: RedisService): Flow[ElemWithToken[T], T, NotUsed] =
    Flow[ElemWithToken[T]]
      .map { elemWithToken =>
        redisService.returnRateLimitToken(key = elemWithToken.key, token = elemWithToken.token)
        elemWithToken.elem
      }
}
