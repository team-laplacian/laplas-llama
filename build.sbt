name := "laplas-llama"

organization := "hr.laplacian.laplas"

scalaVersion := "2.12.8"
crossScalaVersions := Seq("2.11.12", "2.12.8")

libraryDependencies ++= Seq(
  "javax.inject" % "javax.inject" % "1",
  "hr.laplacian.laplas" %% "laplas-redis" % "1.3.0",
  "com.typesafe.akka" %% "akka-stream" % "2.5.18"

)

publishTo := sonatypePublishTo.value

resolvers += Resolver.sonatypeRepo("public")

import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,
  inquireVersions,
  runClean,
  runTest,
  setReleaseVersion,
  commitReleaseVersion,
  tagRelease,
  releaseStepCommand("""sonatypeOpen "hr.laplacian.laplas" "Laplas Staging"""".stripMargin),
  releaseStepCommand("+publishSigned"),
  setNextVersion,
  commitNextVersion,
  releaseStepCommand("sonatypeReleaseAll"),
  pushChanges
)
